﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace stockquotes
{
     public partial class CompanyListViewPage : ContentPage
        {
            public CompanyListViewPage()
            {
                InitializeComponent();
            }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<Company>(this, "SelectedCompany",
                (msg) =>
                {
                    Navigation.PushAsync(new QuoteListViewPage(msg));
                });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<Company>(this, "SelectedCompany");
        }

    }
}
