﻿using System;
using stockquotes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace stockquotes
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new MainPage();
            MainPage = new NavigationPage(new CompanyListViewPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
