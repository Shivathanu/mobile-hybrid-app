﻿using System;
namespace stockquotes
{
    public class Company
    {
        public Company()
        {
        }

        public string Name { get; set; }
        public string Code { get; set; }
    }
}
