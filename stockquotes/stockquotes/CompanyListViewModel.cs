﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace stockquotes
{
    public class CompanyListViewModel
    {
        public List<Company> Companies { get; set; }

        public CompanyListViewModel()
        {
            Companies = new List<Company>
            {
                new Company { Name="Intel", Code="INTC"},
                new Company { Name="Google", Code="GOOG"},
                new Company { Name="Apple", Code="AAPL"},
                new Company { Name="Microsoft", Code="MSFT"},
                new Company { Name="HP", Code="HPQ"},
                new Company { Name="Walmart", Code="WMT"},
            };

        }

        Company selectedCompany;
        public Company SelectedCompany
        {
            get
            {
                return selectedCompany;
            }
            set
            {
                selectedCompany = value;
                if (value != null)
                    MessagingCenter.Send(selectedCompany, "SelectedCompany");
            }
        }

    }
}