﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace stockquotes
{
     public partial class QuoteListViewPage : ContentPage
        {

        public QuoteListViewModel ViewModel { get; set; }

        public QuoteListViewPage(Company company)
        {
            InitializeComponent();
            ViewModel = new QuoteListViewModel(company);
            this.BindingContext = ViewModel;
            
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await this.ViewModel.GetVeiculosWS();

        }



    }
}
