﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace stockquotes
{
    public class QuoteListViewModel
    {
        const string URL_GET_QUOTES = "https://marketdata.websol.barchart.com/";
        const string API_KEY = "4a6088e44b028d8bcb4297799f3735f3";
        const string COMPANY = "GOOG"; //Test
        const string START_DATE = "20200417"; //Test
        const string PATH_GET_QUOTES = "getHistory.json?apikey={0}&symbol={1}&type=daily&%20startDate={2}";
        const int MINUS_DAY = -7;

        public Company Company { get; set; }

        public ObservableCollection<Quote> Quotes { get; set; }

        public QuoteListViewModel(Company company)
        {
            this.Company = company;

            Quotes = new ObservableCollection<Quote>();

            

            //Quotes = new ObservableCollection<Quote>
            //{
            //    new Quote { Company="INTC", TradingDay="2020-04-17",QuoteValue=60.25},
            //    new Quote { Company="INTC", TradingDay="2020-04-20",QuoteValue=59.32},
            //    new Quote { Company="INTC", TradingDay="2020-04-21",QuoteValue=57.43},
            //    new Quote { Company="INTC", TradingDay="2020-04-22",QuoteValue=55.40},
            //    new Quote { Company="INTC", TradingDay="2020-04-23",QuoteValue=59.30},
            //    new Quote { Company="INTC", TradingDay="2020-04-24",QuoteValue=61.10},
            //    new Quote { Company="INTC", TradingDay="2020-04-25",QuoteValue=57.17},
            //    new Quote { Company="INTC", TradingDay="2020-04-26",QuoteValue=55.21},
            //    new Quote { Company="INTC", TradingDay="2020-04-27",QuoteValue=52.20},
            //};

        }

        public async Task GetVeiculosWS()
        {
            
            //Getting date - 7 days
            var date=DateTime.Today.AddDays(MINUS_DAY);
            var date_string = date.ToString("yyyy-MM-dd");

            HttpClient client = new HttpClient();
            var url_format = URL_GET_QUOTES + PATH_GET_QUOTES;
            var url = String.Format(url_format, API_KEY, Company.Code, date_string);
            var result = await client.GetStringAsync(url);
            var JsonObjectResult =JsonConvert.DeserializeObject<MarketDataJSON>(result);
            //Update the Quotes in the build method.
            JsonObjectResult.build(Quotes);
         }

       

    }


    class MarketDataJSON
    {
        [JsonProperty("results")]
        public ObservableCollection<QuoteJSON> JSONquotes { get; set; }
        
        //Receive the quotes list to update from the WS JSON.
        public void build(ObservableCollection<Quote> Quotes)
        {
            //ObservableCollection<Quote> quotes = new ObservableCollection<Quote>();
            foreach (QuoteJSON item in JSONquotes)                
            {
                Quotes.Add(item.Build());
            }

            //return Quotes;
        }
    }

    class QuoteJSON
    {
        public string symbol { get; set; }
        public string tradingDay { get; set; }       
        public double close { get; set; }

        public Quote Build()
        {
            Quote quote = new Quote();
            quote.Company = this.symbol;
            quote.TradingDay = this.tradingDay;
            quote.QuoteValue = this.close;
            return quote;
        }
    }

}