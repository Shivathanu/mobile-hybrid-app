﻿using System;
namespace stockquotes
{
    public class Quote
    {
        public Quote()
        {
        }

        public string Company { get; set; }
        public string TradingDay { get; set; }
        public double QuoteValue { get; set; }
        
    }
}
